var am = angular.module('main_app', [
    'ui.bootstrap',
    'main_app.services',
    'main_app.controllers',
    'ngRoute',
    'ngGrid'
]);

// -----------------------------------------------------------------------------
 
am.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when("/landing", {templateUrl: "src/landing/landing.html", controller: "landing_controller"});

  $routeProvider.when("/profiles", {templateUrl: "src/profiles/profiles.html", controller: "profiles_controller"});
  $routeProvider.when("/profile/:id", {templateUrl: "src/profiles/profile.html", controller: "profile_controller"});
  $routeProvider.when("/profiles_add", {templateUrl: "src/profiles/profiles_add.html", controller: "profiles_add_controller"});

  $routeProvider.when("/vouchers", {templateUrl: "src/vouchers/vouchers.html", controller: "vouchers_controller"});
  $routeProvider.when("/transactions", {templateUrl: "src/transactions/transactions.html", controller: "transactions_controller"});

  $routeProvider.otherwise({redirectTo: '/landing'});  // This is the default route
}]);


// ---------------------------------------------------------------------------

am.run(function($rootScope, init_service) {
  console.log("run");

  $rootScope.lookups = {};
  $rootScope.api = {};

  $rootScope.login = {result : { token:''}};
  $rootScope.session = {};

  init_service.init_api_uris();
  init_service.init_lookup_selectors();
});
//-------------------------------------------------------------------

// am.directive("fileread", [function () {
//     return {
//         scope: {
//             fileread: "="
//         },
//         link: function (scope, element, attributes) {
//             element.bind("change", function (changeEvent) {
//                 scope.$apply(function () {
//                     // scope.fileread = changeEvent.target.files[0];
//                     // or all selected files:
//                     scope.fileread = changeEvent.target.files;
//                 });
//             });
//         }
//     };
// }]);

am.filter('customCurrency', ["$filter", function ($filter) {
    return function(amount, currencySymbol){
        var currency = $filter('currency');
        if(amount < 0){
            return currency(amount, currencySymbol).replace("(", "-").replace(")", ""); 
        }
        return currency(amount, currencySymbol);
    };
}]);

// ------------------------------------------------------------------

var main_services = angular.module('main_app.services', []);
var main_controllers = angular.module('main_app.controllers', []);


