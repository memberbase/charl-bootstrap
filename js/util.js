// Remove the active attribute from a menu item
function navbar_remove_all_active() {
  $('#content_navbar li').each(function() {
    $("#"+this.id).removeClass("active");
  });
}

function validate_form($scope, $rootScope){
  var validate_ok = true;
  $scope.ui.invalid = {};
  return validate_ok;
}

function genran(min,max){ 
  var mx=max;
  var mn=min;
  if(min>max){
    mn=max;
    mx=min;
  }
  var rn = Math.floor(Math.random() * ((mx - mn) + 1) + mn);
  var rnm = rn.toString();
  return rnm;
}

function gen_http($http, success_code, req_obj, error_funct, success_funct){
  var mypromise =  $http(req_obj);
  mypromise.success(function(data, status, headers, config) {
      if (status == success_code) {
          success_funct(data, status);
      } else {
          error_funct(data, status);
      }
  });
  mypromise.error(function(data, status, headers, config) {
    error_funct(data, status);
  });
};
function gen_http_error (data, status){
  var report = '';
  if(status){
    report = report + 'Status: ' + status;
  }
  if(data){
    report = report + 'data: ' + JSON.stringify(data);
  }
  alert(report);
};