
main_controllers.controller('info_controller', 
  function($scope, $rootScope) {
    $scope.rs = $rootScope;
  }
);

main_controllers.controller('menu_controller', 
  function($scope, $rootScope, $location) {
    $scope.rs = $rootScope;

    $scope.logout = function () {
      $rootScope.session = {};
      $rootScope.ui_profiles = {};
      $rootScope.ui_profiles.list = {};
      $location.path('/landing');
    };

    $scope.landing_button = function () {
      $location.path('/landing');
    };
    $scope.landing1_button = function () {
      $location.path('/landing_authed');
    };
    $scope.profiles_button = function () {
      $location.path('/profiles');
    };
    $scope.vouchers_button = function () {
      $location.path('/vouchers');
    };
    $scope.transactions_button = function () {
      $location.path('/transactions');
    };

    // $scope.add_profiles_button = function () {
    //   $location.path('/profiles_add');
    // };
});
