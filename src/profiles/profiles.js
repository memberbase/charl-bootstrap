
main_controllers.controller('profiles_controller', function($http, $scope, $rootScope, $location) {
  navbar_remove_all_active();
  $("#navli_profiles").addClass("active");

  $scope.rs = $rootScope;
  $scope.ui = {};
  $scope.profile_records = {};
  $scope.profile_list = [{rowitem:1}];

  function get_profiles() {
    gen_http(
      $http,
      200,
      {
        method: 'GET',
        url: $rootScope.api_gen + '/profiles', //+ '?offset=' + $scope.list_page,
        params: { 'foobar': new Date().getTime() }
      },
      function (data, status){
        gen_http_error(data, status);
      },
      function (data, status){
        $scope.profile_records = {};
        $scope.profile_list = [];
        if(data.records){
          $scope.profile_records = data.records;
        }
        if(data.records.data){
          $scope.profile_list = data.records.data;
        }
      }
    );
  }
  get_profiles();

  var grid_view_profile_template = '<div>';
  grid_view_profile_template += '<button type="button" class="btn btn-xs"';
  grid_view_profile_template += 'title="View the details of this profile"';
  grid_view_profile_template += 'ng-click="profile_view_button(row.entity[col.field])">';  
  grid_view_profile_template += '<i class="fa fa-arrow-right"></i>';
  grid_view_profile_template += '</button>';
  grid_view_profile_template += '</div>';

  //create the bin button that deletes the individual profile
  var grid_delete_profile_template = '<div>';
  grid_delete_profile_template += '<button type="button" class="btn btn-xs" ';
  grid_delete_profile_template += ' title="Delete this profile from the profile database" ';
  grid_delete_profile_template += 'ng-click="profile_delete_button(row.entity[col.field])">';
  grid_delete_profile_template += '<i class="fa fa-trash-o"></i>';
  grid_delete_profile_template += '</button>';
  grid_delete_profile_template += '</div>';

  $scope.grid_cols_def = [
    {field: 'id', displayName: 'View', width: 50, cellTemplate : grid_view_profile_template, editableCellTemplate : grid_view_profile_template },
    {field: 'id', displayName: 'Profile ID', width: 80},
    {field: 'mobile_number', displayName: 'Mobile number', width: 130},
    {field: 'firstname', displayName: 'First name', width: 130},
    {field: 'lastname', displayName: 'Last name', width: 130},
    {field: 'id', displayName: 'Del', width: 35, cellTemplate : grid_delete_profile_template, editableCellTemplate : grid_delete_profile_template }
  ];

  $scope.grid_filter_options = {
    filterText: ''
  };

  $scope.grid_options = {
    data: 'profile_list',
    enableCellSelection: false,
    enableRowSelection: false,
    enableCellEditOnFocus: true,
    columnDefs: 'grid_cols_def',
    enablePaging: true,
    filterOptions: $scope.grid_filter_options
  };

  $scope.reload_button = function(pid){  
    get_profiles();
  };
  $scope.profile_view_button = function(pid){  
    $location.path('/profile/' + pid );
  };
  $scope.profile_update_button = function(pid){
    $location.path('/profile_update/' + pid);
  };
  $scope.profile_add_button = function(){
    $location.path('/profiles_add');
  };
  $scope.profile_delete_button = function(pid){
    if(confirm("Are you sure you want to delete this profile?") == false) {
      return;
    }
    gen_http(
      $http,
      200,
      {
        method: 'DELETE',
        url: $rootScope.api_gen + '/profiles/' + pid,
        params: { 'foobar': new Date().getTime() }
      },
      function (data, status){
        gen_http_error(data, status);
      },
      function (data, status){
        get_profiles();
      }
    );
  };
})

main_controllers.controller('profiles_add_controller', function($scope, $rootScope, $http, $location) {
  navbar_remove_all_active();
  $("#navli_add_profiles").addClass("active");

  $scope.ui = {};
  $scope.uidirty = false;
  $scope.rs = $rootScope;

  $scope.back_button = function () {
    $location.path('/profiles');
  };

  $scope.random_data_button = function () {
    var rns=genran(1000,9999);
    $scope.ui.firstname = 'firstname-' + rns;
    $scope.ui.lastname = 'lastname-' + rns;
    $scope.ui.mobile_number = 'mobilenumber-' + rns;
  };

  $scope.submit_button = function (formvalid) {
    // if (!formvalid) {
    //   $scope.uidirty = true;
    //   alert('Please check, some entries are not valid. ');
    //   return;
    // }

    var payload = {};
    payload.firstname = $scope.ui.firstname;
    payload.lastname = $scope.ui.lastname;
    payload.mobile_number = $scope.ui.mobile_number;

    gen_http(
      $http,
      201,
      {
        method: 'POST',
        url: $rootScope.api_gen + '/profiles',
        params: { 'foobar': new Date().getTime() },
        data: payload
      },
      function (data, status){
        gen_http_error(data, status);
      },
      function (data, status){
        console.log(JSON.stringify(data));
        $location.path('/profiles');
      }
    );
  };

  // $scope.$watch("ui",function() {
  //   var custom_valid = validate_form($scope, $rootScope);
  // },true);

  // function validate_form($scope, $rootScope){
  //   var validate_ok = true;
  //   $scope.ui.invalid = {};
  //   if (!$scope.ui.accept_tac) {
  //     $scope.ui.invalid.accept_tac = true;
  //     validate_ok = false;
  //   }
  //   return validate_ok;
  // }
});


main_controllers.controller('profile_controller', function($http, $scope, $rootScope, $location, $routeParams) {
  navbar_remove_all_active();
  $("#navli_profiles").addClass("active");

  $scope.ui = {};
  $scope.ui_db = {}; //this is a clone that does not get updated in realtime
  //$scope.uidirty = false;
  $scope.rs = $rootScope;

  $scope.ui_add_account = {};

  function get_data() {
    gen_http(
      $http,
      200,
      {
        method: 'GET',
        url: $rootScope.api_gen + '/profiles/' + $routeParams.id,
        params: { 'foobar': new Date().getTime() }
      },
      function (data, status){
        gen_http_error(data, status);
      },
      function (data, status){
        $scope.ui = data;
        $scope.ui_db = JSON.parse(JSON.stringify(data)); //make a clone that does not get updated
      }
    );
  };
  get_data();

  $scope.reload_button = function () {
    get_data();
  };

  $scope.back_button = function () {
    $location.path('/profiles');
  };

  $scope.submit_button = function (formvalid) {
    var payload = {};
    payload.firstname = $scope.ui.firstname;
    payload.lastname = $scope.ui.lastname;
    payload.mobile_number = $scope.ui.mobile_number;
    payload.gov_id = $scope.ui.gov_id;
    payload.kyc_state = $scope.ui.kyc_state;

    gen_http(
      $http,
      200,
      {
        method: 'PUT',
        url: $rootScope.api_gen + '/profiles/' + $routeParams.id,
        params: { 'foobar': new Date().getTime() },
        data: payload
      },
      function (data, status){
        gen_http_error(data, status);
      },
      function (data, status){
        //console.log(JSON.stringify(data));
        //$location.path('/profiles');
        get_data();
      }
    );
  };

  $scope.add_account_button = function (formvalid) {
    var payload = {};
    payload.type = $scope.ui_add_account.type;

    gen_http(
      $http,
      201,
      {
        method: 'POST',
        url: $rootScope.api_gen + '/profiles/' + $routeParams.id + '/accounts',
        params: { 'foobar': new Date().getTime() },
        data: payload
      },
      function (data, status){
        gen_http_error(data, status);
      },
      function (data, status){
        get_data();
      }
    );
  };




  // -------------------------------------------------------
  var view_transactions_template = '<div>';
  view_transactions_template += '<button type="button" class="btn btn-xs"';
  view_transactions_template += 'ng-click="view_transactions_button(row.entity[col.field])">';  
  view_transactions_template += '<i class="fa fa-arrow-right"></i>';
  view_transactions_template += '</button>';
  view_transactions_template += '</div>';

  $scope.grid_cols_def = [
    {field: 'id', displayName: 'TXs', width: 50, cellTemplate : view_transactions_template, editableCellTemplate : view_transactions_template },
    {field: 'id', displayName: 'ID', width: 80},
    {field: 'type', displayName: 'Type', width: 100},
    {field: 'account_no', displayName: 'Account No', width: 120},
    {field: 'currency', displayName: 'Currency', width: 100},
    {field: 'balance', displayName: 'Balance', width: 80, cellFilter : 'customCurrency : "" : 2', cellClass: 'currency-align'},
    {field: 'name', displayName: 'Name', width: 130}
  ];

  $scope.grid_account_options = {
    data: 'ui.accounts',
    enableCellSelection: false,
    enableRowSelection: false,
    enableCellEditOnFocus: true,
    columnDefs: 'grid_cols_def',
    enablePaging: true
  };

  $scope.view_transactions_button = function(aid){  
    get_transaction(aid);
  };

  // -------------------------------------------------------
  $scope.transaction_records = [];
  $scope.transaction_list = [];

  $scope.grid_cols_def2 = [
    {field: 'id', displayName: 'ID', width: 80},
    {field: 'type', displayName: 'Type', width: 100},
    {field: 'amount', displayName: 'Balance', width: 80, cellFilter : 'customCurrency : "" : 2', cellClass: 'currency-align'},
    {field: 'time', displayName: 'Time', width: 200, cellFilter : 'date : "yyyy-MM-dd HH:mm:ss" : "+0200"'}
  ];

  $scope.grid_transaction_options = {
    data: 'transaction_list',
    enableCellSelection: false,
    enableRowSelection: false,
    enableCellEditOnFocus: true,
    columnDefs: 'grid_cols_def2',
    enablePaging: true
  };

  function get_transaction(aid){
    gen_http(
      $http,
      200,
      {
        method: 'GET',
        url: $rootScope.api_gen + '/accounts/' + aid + '/transactions',
        params: { 'foobar': new Date().getTime() }
      },
      function (data, status){
        gen_http_error(data, status);
      },
      function (data, status){
        $scope.transaction_records = {};
        $scope.transaction_list = [];
        if(data.records){
          $scope.transaction_records = data.records;
        }
        if(data.records.data){
          $scope.transaction_list = data.records.data;
        }
      }
    );
  }


});

