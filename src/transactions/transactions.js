main_controllers.controller('transactions_controller', 
  function($scope, $rootScope, $location, $http) {
    navbar_remove_all_active();
    $("#navli_transactions").addClass("active");

    $scope.rs = $rootScope;
    $scope.ui = {};
    $scope.ui.invalid = {};
    $scope.uidirty = false;

    $scope.submit_button = function(){
		var payload = $scope.ui;

		gen_http(
			$http,
			201,
			{
				method: 'POST',
				url: $rootScope.api_gen + '/transactions',
				params: { 'foobar': new Date().getTime() },
				data: payload
			},
			function (data, status){
				if(status){
					$scope.last_response_status = status;
				}
				if(data){
					$scope.last_response_data = data;
				}
				//gen_http_error(data, status);
			},
			function (data, status){
				//alert('Transaction successfull');
				if(status){
					$scope.last_response_status = status;
				}
				if(data){
					$scope.last_response_data = data;
				}
			}
		);
    }

  }
);

