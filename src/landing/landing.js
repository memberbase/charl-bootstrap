main_controllers.controller('landing_controller', 
  function($scope, $rootScope, $location, $http) {
    navbar_remove_all_active();
    $("#navli_landing").addClass("active");

    $scope.rs = $rootScope;
    $scope.ui = {};
    $scope.ui.invalid = {};
    $scope.uidirty = false;

    $scope.demo_clear_button = function(){
		gen_http(
			$http,
			200,
			{
				method: 'GET',
				url: $rootScope.api_gen + '/demo/clear',
				params: { 'foobar': new Date().getTime() }
			},
			function (data, status){
				gen_http_error(data, status);
			},
			function (data, status){
				alert('Demo data cleared');
			}
		);
    }

    $scope.ui_demo_deposit = {};
    $scope.demo_deposit_button = function(){
		gen_http(
			$http,
			201,
			{
				method: 'POST',
				url: $rootScope.api_gen + '/demo/deposit',
				params: { 'foobar': new Date().getTime() },
				data: $scope.ui_demo_deposit
			},
			function (data, status){
				gen_http_error(data, status);
			},
			function (data, status){
				alert('Deposit successfull');
			}
		);
    }

  }
);

