
main_controllers.controller('vouchers_controller', function($http, $scope, $rootScope, $location) {
  navbar_remove_all_active();
  $("#navli_vouchers").addClass("active");

  $scope.rs = $rootScope;
  $scope.ui = {};
  $scope.voucher_records = {};
  $scope.voucher_list = [{rowitem:1}];

  function get_vouchers() {
    gen_http(
      $http,
      200,
      {
        method: 'GET',
        url: $rootScope.api_gen + '/vouchers', //+ '?offset=' + $scope.list_page,
        params: { 'foobar': new Date().getTime() }
      },
      function (data, status){
        gen_http_error(data, status);
      },
      function (data, status){
        $scope.voucher_records = {};
        $scope.voucher_list = [];
        if(data.records){
          $scope.voucher_records = data.records;
        }
        if(data.records.data){
          $scope.voucher_list = data.records.data;
        }
      }
    );
  }
  get_vouchers();

  $scope.grid_cols_def = [
    {field: 'id', displayName: 'ID', width: 80},
    {field: 'number', displayName: 'Number', width: 130},
    {field: 'value', displayName: 'Value', width: 130},
    {field: 'mobile_number', displayName: 'Mobile Sent', width: 150},
  ];

  $scope.grid_options = {
    data: 'voucher_list',
    enableCellSelection: false,
    enableRowSelection: false,
    enableCellEditOnFocus: true,
    columnDefs: 'grid_cols_def',
    enablePaging: true
  };

  $scope.reload_button = function(pid){  
    get_vouchers();
  };
})



